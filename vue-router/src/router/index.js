import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import signup from '@/components/signup'
import login from '@/components/login'
import more from '@/components/more'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/signup',
      name: 'signup',
      component: signup
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/details/:Iid',
      name: 'more',
      component: more
    }
  ]
})
